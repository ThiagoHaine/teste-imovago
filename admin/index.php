<?php
    $title="Painel Administrativo | Página principal";
    include_once "../layout/connection.php";
    include_once "../layout/header.php";

    if (!is_logado()){
        header("location: /admin/login.php");
    }
?>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1 class="h1">Pagina principal</h1>
                <p>Este é o painel administrativo</p>
            </div>
        </div>
    </div>
<?php
    include_once "../layout/footer.php";
?>