<?php
    $title="Painel administrativo | Login";
    include_once "../layout/connection.php";
    include_once "../layout/header.php";
    $error = 0;

    if (is_logado()){
        header("location: /admin/index.php");
    }

    if (isset($_POST["login"])){
        $senha = md5($_POST["senha"]);
        $query = $con->query("SELECT * FROM USUARIO WHERE login='".$_POST["login"]."' AND senha='".$senha."';");

        if ($query->num_rows>0){
            $row = $query->fetch_assoc();
            $_SESSION["logado"]=$row["nome"];
            header("location: /admin/index.php");
        }else{
            $error=1;
        }
    }
?>
    <div class="container login">
        <div class="row">
            <h1 class="h1">Login</h1>
            <div class="form-group">
                <?php
                    if ($error==1){
                ?>
                    <div class="alert alert-danger">Login ou senha incorretos</div>
                <?php
                    }
                ?>
                <form method="POST">
                    <input type="text" class="form-control" name="login" placeholder="Login" required>
                    <input type="password" class="form-control" name="senha" placeholder="Senha" required>
                    <button type="submit" class="btn btn-success">Logar</button>
                </form>
            </div>
        </div>
    </div>
<?php
    include_once "../layout/footer.php";
?>