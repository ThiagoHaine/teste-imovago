<?php
    $title="Painel Administrativo | Página principal";
    include_once "../layout/connection.php";
    include_once "../layout/header.php";


    if (!is_logado()){
        header("location: /admin/login.php");
        die();
    }

    if (isset($_GET["excluir"])){
        $con->query("DELETE FROM pessoa WHERE id = ".$_GET["excluir"].";");
        header("location: /admin/pessoas.php");
    }

    $busca = "";
    if (isset($_GET["q"])){
        $busca = $_GET["q"];
    }

    $query = $con->query("SELECT * FROM pessoa WHERE nome LIKE '%".$busca."%' or email LIKE '%".$busca."%';");
?>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1 class="h1">Pessoas</h1>
                <form class="row">
                    <div class="col-md-5">
                        <input type="text" class="form-control" name="q" value="<?=$busca?>" placeholder="Buscar por nome ou email">
                    </div>
                    <div class="col-md-1">
                        <button type="submit" class="btn btn-success btn-block">Buscar</button>
                    </div>
                    <div class="col-md-3">
                        <a target="_blank" href="/admin/csv.php?q=<?=$busca?>" type="button" class="btn btn-success btn-block">Exportar CSV</a>
                    </div>
                </form>
                <br>
                <table class="table">
                    <thead>
                        <tr>
                            <th scope="col">Nome</th>
                            <th scope="col">Telefone</th>
                            <th scope="col">E-mail</th>
                            <th scope="col">Foto</th>
                            <th scope="col">Ações</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                            while($row = $query->fetch_assoc()){
                                ?>
                                    <tr>
                                        <th scope="row"><?=$row["nome"]?></th>
                                        <td><?=$row["telefone"]?></td>
                                        <td><?=$row["email"]?></td>
                                        <td><?=$row["foto"]?></td>
                                        <td>
                                            <button class="btn" onclick="excluir('<?=$row['id']?>')"><i class="far fa-trash-alt"></i></button>
                                            <a href="/admin/editar.php?id=<?=$row['id']?>" class="btn"><i class="fas fa-pen"></i></a>
                                        </td>
                                    </tr>
                                <?php
                            }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <script>
        function excluir(id){
            if (confirm("Tem certeza que deseja excluir essa pessoa?")){
                document.location.href=`?excluir=${id}`;
            }
        }
    </script>
<?php
    include_once "../layout/footer.php";
?>