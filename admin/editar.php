<?php
    $title="Painel Administrativo | Página principal";
    include_once "../layout/connection.php";
    include_once "../layout/header.php";

    if (!is_logado()){
        header("location: /admin/login.php");
    }

    if (isset($_POST["nome"])){
        $con->query("UPDATE pessoa SET nome='".$_POST["nome"]."',telefone='".$_POST["telefone"]."',email='".$_POST["email"]."' WHERE id=".$_POST["id"].";");
        header("location: /admin/pessoas.php");
    }

    if (!isset($_GET["id"])){
        header("location: /admin/pessoas.php");
    }

    $query = $con->query("SELECT * FROM pessoa WHERE id=".$_GET["id"]);

    if ($query->num_rows==0){
        header("location: /admin/pessoas.php");
    }

    $row = $query->fetch_assoc();
?>
    <div class="container editar">
        <div class="row">
            <div class="col-md-12">
                <h1 class="h1">Editar</h1>
                <form method="POST" enctype="multipart/form-data">
                    <input type="hidden" name="id" value="<?=$row['id']?>">
                    <div class="form-group">
                        <input type="text" class="form-control" value="<?=$row['nome']?>" placeholder="Nome" name="nome" id="nome" required>
                    </div>
                    <div class="form-group">
                        <input type="tel" class="form-control" value="<?=$row['telefone']?>" placeholder="Telefone" name="telefone" id="telefone" required>
                    </div>
                    <div class="form-group">
                        <input type="email" class="form-control" value="<?=$row['email']?>" placeholder="E-mail" name="email" id="email" required>
                    </div>
                    <button type="submit" class="btn btn-success">Salvar</button>
                </form>
            </div>
        </div>
    </div>
<?php
    include_once "../layout/footer.php";
?>