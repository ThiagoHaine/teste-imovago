<?php
    include_once "../layout/connection.php";

    $busca = "";
    if (isset($_GET["q"])){
        $busca = $_GET["q"];
    }

    $query = $con->query("SELECT * FROM pessoa WHERE nome LIKE '%".$busca."%' or email LIKE '%".$busca."%';");

    $users = array();
    if ($query->num_rows > 0) {
        while ($row = $query->fetch_assoc()) {
            $users[] = $row;
        }
    }

    header('Content-Type: text/csv; charset=utf-8');
    header('Content-Disposition: attachment; filename=Pessoas.csv');
    $output = fopen('php://output', 'w');
    fputcsv($output, array('Id', 'Nome', 'Telefone', 'Email', 'Foto'));

    if (count($users) > 0) {
        foreach ($users as $row) {
            fputcsv($output, $row);
        }
    }
?>