<?php
    $title="Cadastro de Pessoas";
    include_once "layout/connection.php";
    include_once "layout/header.php";
    
    if (isset($_POST["nome"])){
        $target_dir = "images/";
        $imageFileType = strtolower(pathinfo(basename($_FILES["foto"]["name"],PATHINFO_EXTENSION))["extension"]);
        $target_file = $target_dir . $_POST["nome"] . "." . $imageFileType;
        $error=0;

        if (!move_uploaded_file($_FILES["foto"]["tmp_name"], $target_file)){
            $error=1;
        }
        if (!$con->query("INSERT INTO PESSOA(nome,telefone,email,foto) VALUES ('".$_POST["nome"]."','".$_POST["telefone"]."','".$_POST["email"]."','".$target_file."');")){
            $error=1;
        }   
    }
?>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <h1 class="h1">Cadastro de Pessoas</h1>

            <?php
                if (isset($_POST["nome"])){
            ?>
                <div class="alert <?=(($error==0) ? "alert-success" : "alert-danger")?>">
                    <?=(($error==0) ? "Cadastro realizado com sucesso!" : "Erro ao cadastrar")?>
                </div>
            <?php
                }
            ?>
            <form method="POST" enctype="multipart/form-data">
                <div class="form-group">
                    <label for="nome">Nome</label>
                    <input type="text" class="form-control" name="nome" id="nome" required>
                </div>
                <div class="form-group">
                    <label for="nome">E-mail</label>
                    <input type="email" class="form-control" name="email" id="email" required>
                </div>
                <div class="form-group">
                    <label for="nome">Telefone</label>
                    <input type="tel" class="form-control" name="telefone" id="telefone" required>
                </div>
                <div class="form-group">
                    <label for="nome">Foto</label>
                    <input type="file" class="form-control" accept="image/*" name="foto" id="foto" required>
                </div>
                <br>
                <button class="btn btn-success" type="submit">Cadastrar</button>
            </form>
        </div>
    </div>
</div>

<?php
    include_once "layout/footer.php";
?>